using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public int Depth;
    public float Divider;
    public float HeightDivider;

    public int Widht;
    public int Height;
    public float BaseHeight;

    public float Scale;

    public Terrain Terrain;

    public float xOffset;
    public float yOffset;

    // Start is called before the first frame update
    void Start()
    {
        Depth = 200;
        Divider = 12.5f;
        HeightDivider = Depth / Divider;
        Widht = 1000;
        Height = 1000;
        Scale = 20f;
        BaseHeight = ((float)Depth / 2) / Depth;

        xOffset = Random.Range(0, 999999);
        yOffset = Random.Range(0, 999999);

        Terrain = GetComponent<Terrain>();
        Terrain.terrainData = GenerateTerrain(Terrain.terrainData);

        //Terrain.terrainData.size = new Vector3(Widht, FinalDepth, Height);
    }

    TerrainData GenerateTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = Widht + 1;

        terrainData.size = new Vector3(Widht, Depth, Height);

        terrainData.SetHeights(0, 0, GenerateHeights());

        return terrainData;
    }

    float[,] GenerateHeights()
    {
        float[,] heights = new float[Widht, Height];
        SetBaseHeight(heights);
        for (int x = 0; x < Widht; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                heights[x, y] += CalculateHeight(x, y) / HeightDivider;
            }
        }
        SetExtraHeight(heights);
        return heights;
    }

    void SetBaseHeight(float[,] heights)
    {
        for (int x = 0; x < Widht; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                heights[x, y] += BaseHeight;
            }
        }
    }

    void SetExtraHeight(float[,] heights)
    {
        int randomIndexX = Random.Range(0, Widht);
        int rangomIndexY = Random.Range(0, Height);
        Vector2 point = new Vector2(randomIndexX, rangomIndexY);

        for (int x = 0; x < Widht; x ++)
        {
            for (int y = 0; y < Height; y ++)
            {
                Vector2 newPoint = new Vector2(x, y);
                float distance = Vector2.Distance(newPoint, point);

                float cos = 15f*Mathf.Cos(distance * 1f * Mathf.PI / 180);
                if (cos >= 0.1)
                {
                    heights[x, y] *= cos;
                }
            }
        }
    }

    float CalculateHeight(int x, int y)
    {
        float xCoord = (float) x / Widht * Scale + xOffset;
        float yCoord = (float) y / Height * Scale + yOffset;

        return Mathf.PerlinNoise(xCoord, yCoord);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
