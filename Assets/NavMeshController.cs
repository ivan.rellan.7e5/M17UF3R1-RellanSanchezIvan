using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshController : MonoBehaviour
{
    public Transform[] Objetivo;
    public float MaxDistTilTarget;
    private NavMeshAgent _navMeshAgent;
    private int _index;

    // Start is called before the first frame update
    void Start()
    {
        if (MaxDistTilTarget == 0)
            MaxDistTilTarget = 2;

        _index = 0;
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        _navMeshAgent.destination = Objetivo[_index].position;
        float distToTarget = Vector3.Distance(Objetivo[_index].position, transform.position);

        if (distToTarget < MaxDistTilTarget)
        {
            _index++;
            if (_index > Objetivo.Length - 1)
            {
                _index = 0;
            }
        }
    }
}
