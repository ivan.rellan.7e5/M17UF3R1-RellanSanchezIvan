using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aiming : MonoBehaviour
{
    public bool Shooting;
    private Animator _characterAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _characterAnimator = GameObject.Find("PlayerObject").GetComponent<Animator>();
        Shooting = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            Shooting = true;
        }
        else
        {
            Shooting = false;
        }

        if (_characterAnimator != null)
        {
            _characterAnimator.SetBool("Shooting", Shooting);
        }
    }
}
