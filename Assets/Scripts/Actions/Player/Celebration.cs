using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;

public class Celebration : MonoBehaviour
{
    private Animator _characterAnimator;
    private CinemachineFreeLook _standardCinemachine;
    private CinemachineFreeLook _danceCinemachine;
    private CharacterMovement _characterMovement;
    private MonoBehaviour[] _characterScripts;

    // Start is called before the first frame update
    void Start()
    {
        _characterAnimator = GetComponent<Animator>();
        _characterMovement = GetComponentInParent<CharacterMovement>();
        _standardCinemachine = GameObject.Find("StandardCam").GetComponent<CinemachineFreeLook>();
        _danceCinemachine = GameObject.Find("DanceCamera").GetComponent<CinemachineFreeLook>();
        _danceCinemachine.enabled = false;
        _characterScripts = GetComponentsInParent<MonoBehaviour>();
        _characterScripts = _characterScripts.Where(val => val != this).ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.E) && _characterMovement.Direction == Vector3.zero)
        {
            foreach (MonoBehaviour script in _characterScripts)
            {
                script.enabled = false;
            }
            _standardCinemachine.enabled = false;
            _danceCinemachine.enabled = true;
            _characterAnimator.SetBool("Dance", true);
        }
    }

    public void OnDanceEnded()
    {
        foreach (MonoBehaviour script in _characterScripts)
        {
            script.enabled = true;
        }
        _standardCinemachine.enabled = true;
        _danceCinemachine.enabled = false;
        _characterAnimator.SetBool("Dance", false);
    }
}
