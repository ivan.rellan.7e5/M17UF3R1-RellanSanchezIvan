using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Using : MonoBehaviour
{
    private Animator _characterAnimator;
    private MonoBehaviour[] _characterScripts;

    // Start is called before the first frame update
    void Start()
    {
        _characterAnimator = GetComponent<Animator>();
        _characterScripts = GetComponentsInParent<MonoBehaviour>();
        _characterScripts = _characterScripts.Where(val => val != this).ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            foreach (MonoBehaviour script in _characterScripts)
            {
                script.enabled = false;
            }
            _characterAnimator.SetBool("Using", true);
        }
    }

    public void OnUsed()
    {
        foreach (MonoBehaviour script in _characterScripts)
        {
            script.enabled = true;
        }
        _characterAnimator.SetBool("Using", false);
    }
}
