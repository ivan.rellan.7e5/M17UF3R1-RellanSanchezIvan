using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Melee", menuName = "Melee")]
public class MeleeScriptableObject : WeaponScriptableObject
{
    public float Range;
    public float Refresh;
}
