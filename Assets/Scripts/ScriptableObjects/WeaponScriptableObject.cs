using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScriptableObject : ItemScriptableObject
{
    public float Damage;
}
