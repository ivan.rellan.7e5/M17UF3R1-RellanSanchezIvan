using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class GunScriptableObject : WeaponScriptableObject
{
    public float Distance;
    public float Cadency;
    public float MaxBullets;
}
