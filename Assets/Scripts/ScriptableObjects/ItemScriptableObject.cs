using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class ItemScriptableObject : ScriptableObject
{
    public Mesh ItemMesh;
    public int Points;
    public float Price;
    public Quaternion Rotation;
}
