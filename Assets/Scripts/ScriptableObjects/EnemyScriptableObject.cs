using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Zombie", menuName = "Zombie")]
public class EnemyScriptableObject : ScriptableObject
{
    public float StartWaitTime;
    public float TimeToRotate;
    public float SpeedWalk;
    public float SpeedRun;
    public float ViewRadius;
    public float ViewAngle;
    public float MeshResolution;
    public int EdgeIterations;
    public float EdgeDistance;
}
