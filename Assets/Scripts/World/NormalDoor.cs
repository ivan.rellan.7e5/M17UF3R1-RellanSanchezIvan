using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NormalDoor : MonoBehaviour
{
    public string SceneName;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        /*foreach(ContactPoint contact in collision.contacts)
        {
            CharacterMovement player = contact.otherCollider.GetComponent<CharacterMovement>();
            if (player)
            {
                SceneManager.LoadScene("R1.2.2 Interiors");
            }
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterMovement player = other.gameObject.GetComponent<CharacterMovement>();
        if (player)
        {
            SceneController.GoToScene(SceneName);
        }
    }
}
