using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum PatrolStatus
{
    Patrol,
    TargetInRange,
    TargetNear
}

public class Zombie : MonoBehaviour
{
    public EnemyScriptableObject Data;
    public NavMeshAgent NavMeshAgent;
    public LayerMask PlayerMask;
    public LayerMask ObstacleMask;
    private int _currentWayPoint;
    private Vector3 _playerLastPosition;
    private Vector3 _playerPosition;
    private float _waitTime;
    private float _timeToRotate;
    private PatrolStatus _patrolStatus;
    public Transform[] WayPoints;

    // Start is called before the first frame update
    void Start()
    {
        _playerPosition = Vector3.zero;
        _playerLastPosition = Vector3.zero;
        _currentWayPoint = 0;
        NavMeshAgent = GetComponent<NavMeshAgent>();
        NavMeshAgent.isStopped = false;
        NavMeshAgent.speed = Data.SpeedWalk;
        SetDefaults();
    }

    // Update is called once per frame
    void Update()
    {
        EnviromentView();
        
        if (_patrolStatus != PatrolStatus.Patrol)
        {
            Chasing();
        }
        else
        {
            Patroling();
        }
    }

    void SetDefaults()
    {
        NavMeshAgent.SetDestination(WayPoints[_currentWayPoint].position);
        _waitTime = Data.StartWaitTime;
        _timeToRotate = Data.TimeToRotate;
    }

    void OnTargetSpotted(Vector3 targetPosition)
    {
        NavMeshAgent.SetDestination(targetPosition);
        if (Vector3.Distance(transform.position, targetPosition) <= 0.3)
        {
            CheckWaitTime();
        }
    }

    void EnviromentView()
    {
        Collider[] playerInRange = Physics.OverlapSphere(transform.position, Data.ViewRadius, PlayerMask);

        foreach (Collider col in playerInRange)
        {
            Transform player = col.transform;
            Vector3 dirToPlayer = (player.position - transform.position).normalized;

            if (Vector3.Angle(transform.position, dirToPlayer) < Data.ViewAngle / 2)
            {
                float dstToPlayer = Vector3.Distance(transform.position, player.position);
                if (!Physics.Raycast(transform.position, dirToPlayer, dstToPlayer, ObstacleMask))
                {
                    _patrolStatus = PatrolStatus.TargetInRange;
                }
                else
                {
                    _patrolStatus = PatrolStatus.Patrol;
                }
            }

            if (Vector3.Distance(transform.position, player.position) > Data.ViewRadius)
            {
                _patrolStatus = PatrolStatus.Patrol;
            }

            if (_patrolStatus == PatrolStatus.TargetInRange)
            {
                _playerPosition = player.transform.position;
            }
        }
    }

    void Patroling()
    {
        if (_patrolStatus == PatrolStatus.TargetNear)
        {
            if (_timeToRotate <= 0)
            {
                Move(Data.SpeedWalk);
                OnTargetSpotted(_playerLastPosition);
            }
            else
            {
                Stop();
                _timeToRotate -= Time.deltaTime;
            }
        }
        else
        {
            _patrolStatus = PatrolStatus.Patrol;
            _playerLastPosition = Vector3.zero;
            NavMeshAgent.SetDestination(WayPoints[_currentWayPoint].position);
            if (NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance)
            {
                if (_waitTime <= 0)
                {
                    NextPoint();
                    Move(Data.SpeedWalk);
                    _waitTime = Data.StartWaitTime;
                }
                else
                {
                    Stop();
                    _timeToRotate = Data.TimeToRotate;
                }
            }
        }
    }

    void Chasing()
    {
        _patrolStatus = PatrolStatus.Patrol;
        _playerLastPosition = Vector3.zero;

        Move(Data.SpeedRun);
        NavMeshAgent.SetDestination(_playerPosition);
        if (NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance)
        {
            if (_waitTime <= 0 && Vector3.Distance(transform.position, GameObject.Find("Player").transform.position) >= 6f)
            {
                _patrolStatus = PatrolStatus.Patrol;
                Move(Data.SpeedWalk);
                SetDefaults();
            }
            else
            {
                if (Vector3.Distance(transform.position, GameObject.Find("Player").transform.position) >= 2.5f)
                {
                    Stop();
                    _waitTime -= Time.deltaTime;
                }
            }
        }
    }

    void CheckWaitTime()
    {
        if (_waitTime <= 0)
        {
            Move(Data.SpeedWalk);
            SetDefaults();
        }
        else
        {
            Stop();
            _waitTime -= Time.deltaTime;
        }
    }

    void Move(float speed)
    {
        NavMeshAgent.isStopped = false;
        NavMeshAgent.speed = speed;
    }

    void Stop()
    {
        NavMeshAgent.isStopped = true;
        NavMeshAgent.speed = 0f;
    }

    void NextPoint()
    {
        _currentWayPoint = (_currentWayPoint + 1) % WayPoints.Length;
        NavMeshAgent.SetDestination(WayPoints[_currentWayPoint].position);
    }
}
