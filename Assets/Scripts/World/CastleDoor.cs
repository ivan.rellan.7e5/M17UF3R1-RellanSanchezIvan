using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleDoor : MonoBehaviour
{
    public ItemScriptableObject KeyData;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInventoryManager Inventory = other.GetComponent<PlayerInventoryManager>();
        Debug.Log(Inventory);
        if (Inventory)
        {
            if (Inventory.PlayerInventory.CurrentWeapons.Contains(KeyData))
                GameManager.Instance.OnVictory();
            else
            {
                _audioSource.Play();
                GameManager.Instance.OnFailedAttempt();
            }
        }
    }
}
