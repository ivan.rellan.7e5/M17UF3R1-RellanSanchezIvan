using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Victory,
    Defeated
}

public class GameManager : MonoBehaviour
{
    public GameState GameState;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("PlayerManager is NULL");
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(_instance.gameObject);
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnVictory()
    {
        GameState = GameState.Victory;
        ToGameOverScene();
    }

    public void OnFailedAttempt()
    {
        Debug.Log("You need the key!");
    }

    public void OnFailure()
    {
        GameState = GameState.Defeated;
        ToGameOverScene();
    }

    void ToGameOverScene()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneController.GoToScene("R1.3.3 - GameOver");
    }
}
