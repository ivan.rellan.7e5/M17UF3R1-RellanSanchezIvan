using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemScriptableObject ItemData;
    public AudioSource AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInventoryManager playerInventory = other.GetComponent<PlayerInventoryManager>();
        if (playerInventory)
        {
            playerInventory.AddToInventory(ItemData);
            GetComponent<CapsuleCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
            PlaySound();
            Destroy(gameObject, 2f);
        }
            
    }

    void PlaySound()
    {
        if (AudioSource)
            AudioSource.Play();
    }
}
