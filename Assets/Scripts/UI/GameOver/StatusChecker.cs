using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusChecker : MonoBehaviour
{
    private Text _text;
    public Dictionary<GameState, string> StatusDictionary;

    // Start is called before the first frame update
    void Start()
    {
        _text = GetComponent<Text>();
        StatusDictionary = new Dictionary<GameState, string>()
        {
            { GameState.Victory, "YOU WON!" },
            { GameState.Defeated, "GAME OVER" }
        };

        if (GameManager.Instance != null)
        {
            CheckStatus();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CheckStatus()
    {
        _text.text = StatusDictionary[GameManager.Instance.GameState];
        if (GameManager.Instance.GameState == GameState.Victory)
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
