using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour, IDamageble
{
    public void TakeDamage()
    {
        GameManager.Instance.OnFailure();
    }
}
