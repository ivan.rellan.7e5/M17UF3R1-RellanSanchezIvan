﻿using BehaviourTree;
using UnityEngine;

class CheckTargetInFOVRange : Node
{
    private static int _targetLayerMask = LayerMask.GetMask("Player");

    private Transform _transform;

    public CheckTargetInFOVRange(Transform transform)
    {
        _transform = transform;
    }

    public override State Evaluate()
    {
        Collider[] colliders = Physics.OverlapSphere(
                _transform.position,
                EnemyBehaviourTree.FovRange,
                _targetLayerMask
        );

        if (colliders.Length > 1)
        {
            if (Parent.Parent.GetData("target") == null)
            {
                Parent.Parent.SetData("target", colliders[0].transform);
            }

            EnemyBehaviourTree.OwnAnimator.SetBool("Chasing", true);
            state = State.SUCCES;
            return state;
        }
        else
        {
            if (Parent.Parent.GetData("target") != null)
            {
                Parent.Parent.ClearData("target");
            }

            EnemyBehaviourTree.OwnAnimator.SetBool("Chasing", false);
            state = State.FAILURE;
            return state;
        } 
    }
}

