﻿using BehaviourTree;
using UnityEngine;

class TaskAttackTarget : Node
{
    private static int _targetLayerMask = LayerMask.GetMask("Player");

    private Transform _transform;
    private Transform _targetTransform;
    private IDamageble _target;

    private float _waitTilNextAttack = 2f;
    private float _waitTime;
    private float _waitUntilAttackEffect = 1.3f;
    private float _actualWaitingTimeTilAttackEffect;

    private bool _attacking = true;

    public TaskAttackTarget(Transform transform)
    {
        _transform = transform;
    }

    public override State Evaluate()
    {
        _target = ((Transform)GetData("target")).GetComponent<IDamageble>();
        _targetTransform = (Transform)GetData("target");

        if (_target == null)
        {
            state = State.FAILURE;
            return state;
        }

        if (_attacking == false)
        {
            _waitTime += Time.deltaTime;
            if (_waitTime >= _waitTilNextAttack)
            {
                _attacking = true;
                _waitTime = 0f;
            }
        }
        else
        {
            if (_actualWaitingTimeTilAttackEffect >= _waitUntilAttackEffect)
            {
                _actualWaitingTimeTilAttackEffect = 0;
                if (Vector3.Distance(_transform.position, _targetTransform.position) <= EnemyBehaviourTree.AttackRange)
                {
                    _target.TakeDamage();
                }
            }
            else
            {
                _actualWaitingTimeTilAttackEffect += Time.deltaTime;
            }
        }

        state = State.RUNNING;
        return state;
    }
}