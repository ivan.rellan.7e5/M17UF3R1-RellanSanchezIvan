﻿using BehaviourTree;
using UnityEngine;

class CheckTargetInAttackRange : Node
{
    private Transform _transform;

    public CheckTargetInAttackRange(Transform transform)
    {
        _transform = transform;
    }

    public override State Evaluate()
    {
        Transform target = (Transform) GetData("target");

        if (target != null && target.GetComponent<IDamageble>() != null)
        {
            if (Vector3.Distance(_transform.position, target.position) <= EnemyBehaviourTree.AttackRange)
            {
                EnemyBehaviourTree.OwnAnimator.SetBool("Attacking", true);
                state = State.SUCCES;
                return state;
            }
        }

        EnemyBehaviourTree.OwnAnimator.SetBool("Attacking", false);
        state = State.FAILURE;
        return state;
    }
}

