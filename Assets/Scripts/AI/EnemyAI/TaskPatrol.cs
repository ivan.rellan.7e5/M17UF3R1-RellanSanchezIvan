using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using BehaviourTree;

public class TaskPatrol : Node
{
    private Transform _transform;
    private Transform[] _waypoints;
    private NavMeshAgent _navMeshAgent;

    private int _index = 0;

    private float _waitTime = 1f;
    private float _waitCounter = 0f;
    private bool _waiting = false;

    public TaskPatrol(Transform transform, Transform[] waypoints, NavMeshAgent navMeshAgent)
    {
        _transform = transform;
        _waypoints = waypoints;
        _navMeshAgent = navMeshAgent;
        _navMeshAgent.speed = EnemyBehaviourTree.Speed;
    }

    public override State Evaluate()
    {
        if (_waiting)
        {
            EnemyBehaviourTree.OwnAnimator.SetBool("Patrolling", false);
            _waitCounter += Time.deltaTime;
            if (_waitCounter >= _waitTime)
                _waiting = false;
        }
        else
        {
            EnemyBehaviourTree.OwnAnimator.SetBool("Patrolling", true);
            Transform wp = _waypoints[_index];

            _navMeshAgent.destination = wp.position;
            float distToTarget = Vector3.Distance(wp.position, _transform.position);

            if (distToTarget < 1f)
            {
                _transform.position = wp.position;
                _waitCounter = 0f;
                _waiting = true;
                _index = (_index + 1) % _waypoints.Length;
            }
            else
            {
                _navMeshAgent.destination = wp.position;
            }
        }

        

        state = State.RUNNING;
        return state;
    }
}
