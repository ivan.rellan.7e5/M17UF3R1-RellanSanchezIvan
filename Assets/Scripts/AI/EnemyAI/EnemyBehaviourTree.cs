﻿using BehaviourTree;
using System.Collections.Generic;
using UnityEngine;


class EnemyBehaviourTree : BehaviourTree.Tree
{
    public Transform[] Waypoints;
    public UnityEngine.AI.NavMeshAgent NavMeshAgent;

    public static float Speed = 3f;
    public static float RunSpeed = 6f;
    public static float FovRange = 8f;
    public static float AttackRange = 3f;
    [SerializeField] public static Animator OwnAnimator;

    override protected void Start()
    {
        base.Start();
        OwnAnimator = GetComponentInChildren<Animator>();
    }

    protected override Node SetUpTree()
    {
        Node root = new Selector(new List<Node>
        {
            new Sequence(new List<Node>
            {
                new CheckTargetInAttackRange(transform),
                new TaskAttackTarget(transform)
            }),

            new Sequence(new List<Node>
            {
                new CheckTargetInFOVRange(transform),
                new TaskFollowTarget(transform, NavMeshAgent)
            }),

            new TaskPatrol(transform, Waypoints, NavMeshAgent)
        });

        return root;
    }
}

