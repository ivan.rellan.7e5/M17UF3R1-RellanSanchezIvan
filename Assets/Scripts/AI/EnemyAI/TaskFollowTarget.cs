﻿using BehaviourTree;
using UnityEngine;
using UnityEngine.AI;

class TaskFollowTarget : Node
{
    private Transform _transform;
    private NavMeshAgent _navMeshAgent;

    public TaskFollowTarget(Transform transform, NavMeshAgent navMeshAgent)
    {
        _transform = transform;
        _navMeshAgent = navMeshAgent;
        _navMeshAgent.speed = EnemyBehaviourTree.RunSpeed;
    }

    public override State Evaluate()
    {
        Transform target = (Transform) GetData("target");
        
        if (Vector3.Distance(_transform.position, target.position) > 0.01f)
        {
            /*_transform.position = Vector3.MoveTowards(
                _transform.position, target.position, EnemyBehaviourTree.Speed * Time.deltaTime);*/

            _navMeshAgent.SetDestination(target.position);

            _transform.LookAt(target.transform);
        }

        state = State.RUNNING;
        return state;
    }
}

