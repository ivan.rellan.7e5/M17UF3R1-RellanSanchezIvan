using System.Collections.Generic;

namespace BehaviourTree
{
    public class Sequence : Node
    {
        public Sequence() : base() { }
        public Sequence(List<Node> children) : base(children) { }

        public override State Evaluate()
        {
            bool anyChildIsRunning = false;

            foreach (Node child in Children)
            {
                switch (child.Evaluate())
                {
                    case State.FAILURE:
                        state = State.FAILURE;
                        return state;
                    case State.SUCCES:
                        continue;
                    case State.RUNNING:
                        anyChildIsRunning = true;
                        continue;
                    default:
                        state = State.SUCCES;
                        return state;
                }
            }

            state = anyChildIsRunning ? State.RUNNING : State.SUCCES;
            return state;
        }
    }
}
