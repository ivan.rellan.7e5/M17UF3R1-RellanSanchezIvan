using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Node", menuName = "Node", order = 3)]
public class NodeScriptableObject : ScriptableObject
{
    public NodeScriptableObject Parent;
    public List<NodeScriptableObject> Children;
    public ConditionScriptableObject Condition;
    public ActionScriptableObject Action;
}
