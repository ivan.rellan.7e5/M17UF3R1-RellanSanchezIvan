using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    /*public enum CameraState
    {
        Survival,
        Combat
    }*/

    public CinemachineFreeLook StandardCamSettings;
    //public GameObject CombatCameraSettings;
    public Transform CombatTarget;
    public Transform PlayerTarget;
    public float StandardFov;
    public float CombatFov;
    private float _fovChange;
    //public bool CombatMode;

    /*public CameraState State;
    public Transform Orientation;
    public Transform Player;
    public Transform PlayerObject;
    public Rigidbody Rigidbody;
    public Transform CombatTarget;
    public float RotationSpeed;*/

    // Start is called before the first frame update
    void Start()
    {
        /*StandardCamSettings.SetActive(false);
        CombatCameraSettings.SetActive(false);*/
        //CombatMode = false;
        StandardFov = 50;
        CombatFov = 25;
        _fovChange = StandardFov;
        CombatTarget = GameObject.Find("CombatTarget").transform;
        PlayerTarget = GameObject.Find("Player").transform;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            StandardCamSettings.LookAt = CombatTarget;
            _fovChange = Mathf.Clamp(_fovChange - 1, CombatFov, StandardFov);
            StandardCamSettings.m_Lens.FieldOfView = _fovChange;
            //StandardCamSettings.transform.position = CombatCameraSettings.transform.position;
        }
        else
        {
            StandardCamSettings.LookAt = PlayerTarget;
            _fovChange = Mathf.Clamp(_fovChange + 1, CombatFov, StandardFov);
            StandardCamSettings.m_Lens.FieldOfView = _fovChange;
            //CombatCameraSettings.transform.position = StandardCamSettings.transform.position;
        }

        /*StandardCamSettings.SetActive(!CombatMode);
        CombatCameraSettings.SetActive(CombatMode);*/
        

        /*var camDirection = (Player.position - new Vector3(transform.position.x, Player.position.y, transform.position.z)).normalized;
        Orientation.forward = camDirection;

        if (State == CameraState.Combat)
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");
            Vector3 inputDirection = Orientation.forward * verticalInput + Orientation.forward * horizontalInput;

            if (inputDirection != Vector3.zero)
                PlayerObject.forward = Vector3.Slerp(PlayerObject.forward, inputDirection.normalized, Time.deltaTime * RotationSpeed);
        }
        else
        {
            var combatCamDirection = (CombatTarget.position - new Vector3(transform.position.x, CombatTarget.position.y, transform.position.z)).normalized;
            Orientation.forward = combatCamDirection;
        }*/

    }
}
