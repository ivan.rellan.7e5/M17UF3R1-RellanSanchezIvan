using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    public float JumpForce;
    private CharacterController _controller;
    private CharacterMovement _chracterMovement;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _chracterMovement = GetComponent<CharacterMovement>();
        if (JumpForce == 0)
        {
            JumpForce = 15;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _controller.isGrounded)
        {
            _controller.attachedRigidbody.velocity += Vector3.up * JumpForce;
        }
    }
    /*IEnumerator Jumping(bool isGrounded)
    {
        _controller.attachedRigidbody.velocity -= transform.position.y * 9.81 * Time.deltaTime;
        yield return new WaitUntil(() => _controller.isGrounded);
    }*/
}
