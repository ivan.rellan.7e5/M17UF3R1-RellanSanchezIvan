using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationController : MonoBehaviour
{
    public Transform Camera;
    public float Sensitivity;
    private Animator _characterAnimator;


    // Start is called before the first frame update
    void Start()
    {
        if (Sensitivity == 0)
        {
            Sensitivity = .5f;
        }

        _characterAnimator = GameObject.Find("PlayerObject").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_characterAnimator.GetBool("Dance"))
        {
            var direction = (transform.position - new Vector3(Camera.transform.position.x, transform.position.y, Camera.transform.position.z)).normalized;
            transform.forward = direction;
        }
        
        /*_turningAround.x += Input.GetAxis("Mouse X") * Sensitivity;
        _turningAround.y += Input.GetAxis("Mouse Y") * Sensitivity;
        transform.rotation = Quaternion.Euler(0, _turningAround.x, 0);*/
    }
}
