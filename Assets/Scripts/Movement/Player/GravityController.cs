using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

enum GravityState
{
    Gounded,
    Jumping,
    Falling
}

public class GravityController : MonoBehaviour
{
    public float Gravity;
    public float JumpForce;
    public float JumpHeight;
    public float MaxNonFallingTime;
    private float _fallingTime;
    private float _verticalSpeed;
    private float _terminalSpeed;
    private GravityState _grounded;
    private CharacterMovement _characterMovement;
    private Animator _characterAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _characterMovement = GetComponent<CharacterMovement>();
        _characterAnimator = GameObject.Find("PlayerObject").GetComponent<Animator>();

        if (Gravity == 0)
            Gravity = 25f;

        if (JumpForce == 0)
            JumpForce = 30;

        if (JumpHeight == 0)
            JumpHeight = 1.8f;

        if (_terminalSpeed == 0)
            _terminalSpeed = 53f;

        if (MaxNonFallingTime == 0)
            MaxNonFallingTime = .4f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ApplyGravity();
    }

    void ApplyGravity()
    {
        if (Physics.CheckSphere(transform.position, 0.1f) && _grounded != GravityState.Jumping)
        {
            _grounded = GravityState.Gounded;
        }
        else
        {
            _grounded = GravityState.Falling;
        }

        if (_grounded == GravityState.Gounded)
        {
            _fallingTime = 0;
            _verticalSpeed = 0;
            if (_characterAnimator != null)
            {
                _characterAnimator.SetBool("Grounded", true);
                _characterAnimator.SetBool("Jumped", false);
                _characterAnimator.SetBool("Falling", false);
            }
        }
        else
        {
            FallingController();
        }

        _characterMovement.VerticalSpeed = _verticalSpeed;
    }

    public void Jump(InputAction.CallbackContext _)
    {
        if (_grounded == GravityState.Gounded)
        {
            _grounded = GravityState.Jumping;
            if (_characterAnimator != null)
            {
                _characterAnimator.SetBool("Grounded", false);
                _characterAnimator.SetBool("Jumped", true);
            }
            _verticalSpeed = Mathf.Sqrt(JumpHeight * 2f * Gravity);
        }
    }

    void FallingController()
    {
        _fallingTime += Time.deltaTime;
        if (_characterAnimator != null && _fallingTime >= MaxNonFallingTime)
        {
            _characterAnimator.SetBool("Grounded", false);
            _characterAnimator.SetBool("Falling", true);
        }

        _verticalSpeed -= Time.deltaTime * Gravity;
    }
}
