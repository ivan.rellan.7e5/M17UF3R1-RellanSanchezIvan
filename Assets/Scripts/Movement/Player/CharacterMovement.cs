using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float Speed;
    public float RunSpeed;
    public float CrouchSpeed;
    public float MaxAnimationSpeedWalking;
    public float MaxAnimationSpeedRunning;
    public float VerticalSpeed;
    public Vector3 Direction;
    private float _horizontalMovement;
    private float _verticalMovement;
    private CharacterController _controller;
    private Animator _characterAnimator;

    // Start is called before the first frame update
    void Start()
    {
        SetUp();
    }

    // Update is called once per frame
    void Update()
    {
        Inputs();
    }

    private void FixedUpdate()
    {
        Move();
    }

    void SetUp()
    {
        _controller = GetComponent<CharacterController>();
        _characterAnimator = GameObject.Find("PlayerObject").GetComponent<Animator>();
        _controller.enabled = true;
        if (Speed == 0)
        {
            Speed = 10;
        }

        if (RunSpeed == 0)
        {
            RunSpeed = 20;
        }

        if (CrouchSpeed == 0)
        {
            CrouchSpeed = 5f;
        }

        if (MaxAnimationSpeedWalking == 0)
        {
            MaxAnimationSpeedWalking = .5f;
        }

        if (MaxAnimationSpeedRunning == 0)
        {
            MaxAnimationSpeedRunning = 1f;
        }
    }

    void Move()
    {
        float SpeedMultiplier = Speed;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            SpeedMultiplier = RunSpeed;
        }
        else if (_characterAnimator.GetBool("Crouch"))
        {
            SpeedMultiplier = CrouchSpeed;
        }

        Direction = (transform.right * _horizontalMovement + transform.forward * _verticalMovement) * SpeedMultiplier;
        Direction.y = VerticalSpeed;
        _controller.Move(Direction * Time.deltaTime);

        if (Direction != Vector3.zero)
        {
            _characterAnimator.SetFloat("Speed", SpeedMultiplier/RunSpeed);
        }
        else
        {
            _characterAnimator.SetFloat("Speed", 0f);
        }
    }

    void Inputs()
    {
        _horizontalMovement = Input.GetAxis("Horizontal");
        _verticalMovement = Input.GetAxis("Vertical");
    }
}
