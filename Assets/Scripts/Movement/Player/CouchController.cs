using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CouchController : MonoBehaviour
{
    private CharacterMovement _characterMovement;
    private CharacterController _controller;
    private Animator _characterAnimator;
    private float _actualSpeed;
    public float CrouchSpeed;
    

    // Start is called before the first frame update
    void Start()
    {
        _characterMovement = GetComponent<CharacterMovement>();
        _controller = GetComponent<CharacterController>();
        _characterAnimator = GameObject.Find("PlayerObject").GetComponent<Animator>();

        if (CrouchSpeed == 0)
        {
            CrouchSpeed = 5;
        }

        //_actualSpeed = _characterMovement.Speed;
    }

    // Update is called once per frame
    void Update()
    {
        //_characterMovement.Speed = _actualSpeed;
    }

    public void CrouchController(InputAction.CallbackContext _)
    {
        if (_characterAnimator != null)
        {
            _characterAnimator.SetBool("Crouch", true);
        }
    }

    public void CrouchCanceled(InputAction.CallbackContext _)
    {
        if (_characterAnimator != null)
        {
            _characterAnimator.SetBool("Crouch", false);
        }
    }
}
