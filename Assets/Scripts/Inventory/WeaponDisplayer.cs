using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDisplayer : MonoBehaviour
{
    public Inventory PlayerInventory;
    public Material Material;

    // Start is called before the first frame update
    void Start()
    {
        PlayerInventory = FindObjectOfType<PlayerInventoryManager>().PlayerInventory;
        if (PlayerInventory.CurrentWeapons.Count > 0)
            foreach(var i in PlayerInventory.CurrentWeapons)
            {
                DisplayItem(i);
            }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayItem(ItemScriptableObject Item)
    {
        GameObject go = new GameObject();
        go.transform.rotation = Item.Rotation;
        go.AddComponent<MeshFilter>();
        go.GetComponent<MeshFilter>().mesh = Item.ItemMesh;
        go.AddComponent<MeshRenderer>();
        go.GetComponent<MeshRenderer>().material = Material;
        Instantiate(go, transform);
    }
}
