using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventoryManager : MonoBehaviour
{
    public Inventory PlayerInventory;
    private WeaponDisplayer _weaponDisplayer;

    // Start is called before the first frame update
    void Start()
    {
        _weaponDisplayer = GameObject.Find("WeaponHolder").GetComponent<WeaponDisplayer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddToInventory(ItemScriptableObject Item)
    {
        PlayerInventory.CurrentWeapons.Add(Item);
        _weaponDisplayer.DisplayItem(Item);
    }

    public void RemoveFromInventory(ItemScriptableObject Item)
    {
        PlayerInventory.CurrentWeapons.Remove(Item);
    }

    public void RemoveFromInventory(int Index)
    {
        PlayerInventory.CurrentWeapons.RemoveAt(Index);
    }
}
