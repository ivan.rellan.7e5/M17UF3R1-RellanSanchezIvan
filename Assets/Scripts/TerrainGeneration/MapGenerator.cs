using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum DrawMode
    {
        NoiseMap,
        ColorMap
    }

    public DrawMode DMode;

    public int MapWidth;
    public int MapHeight;
    public float NoiseScale;
    public int Octaves;
    [Range(0, 1)]
    public float Persistance;
    public float Lacunarity;
    public int Seed;
    public Vector2 Offset;

    public TerrainType[] Regions;

    public bool AutoUpdate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateMap()
    {
        float[,] noiseMap = NoiseGenerator.GenerateNoiseMap(MapWidth, MapHeight, NoiseScale, Seed, Octaves, Persistance, Lacunarity, Offset);
        Color[] colorMap = new Color[MapWidth * MapHeight];

        for (var y = 0; y < MapHeight; y++)
        {
            for (var x = 0; x < MapWidth; x++)
            {
                float currentHeight = noiseMap[x, y];
                foreach (TerrainType i in Regions)
                {
                    if (currentHeight <= i.Height)
                    {
                        colorMap[y * MapWidth + x] = i.Color;
                        break;
                    }
                }
            }
        }

        MapDisplay mapDisplay = FindObjectOfType<MapDisplay>();
        if (DMode == DrawMode.NoiseMap)
            mapDisplay.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        else if (DMode == DrawMode.ColorMap)
            mapDisplay.DrawTexture(TextureGenerator.TextureFromColorMap(colorMap, MapWidth, MapHeight));
    }

    private void OnValidate()
    {
        if (MapWidth < 1)
        {
            MapWidth = 1;
        }
        if (MapHeight < 1)
        {
            MapHeight = 1;
        }
        if (Lacunarity < 1)
        {
            Lacunarity = 1;
        }
        if (Octaves < 0)
        {
            Octaves = 0;
        }
    }

    [System.Serializable]
    public struct TerrainType
    {
        public string Label;
        public float Height;
        public Color Color;
    }
}
