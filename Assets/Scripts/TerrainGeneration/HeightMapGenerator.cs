using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    public static void GenerateTerrainMesh(float[,] heightMap)
    {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);

        
    }
}

[RequireComponent(typeof(MeshFilter))]
public class HeightMapGenerator : MonoBehaviour
{
    public Terrain Terrain;
    private float[,] _perlinNoiseMap;

    public Mesh Mesh;
    public int xSize;
    public int zSize;

    private Vector3[] _vertices;
    private int[] _triangles;
    private float _randomSeed;
    private float _newPerlinX;
    private float _newPerlinZ;

    // Start is called before the first frame update
    void Start()
    {
        xSize = 250;
        zSize = 250;

        Debug.Log(_randomSeed);

        if (Mesh == null)
        {
            Mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = Mesh;
        }

        CreateShape();
        UpdateMesh();
    }

    void CreateShape()
    {
        _vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        _triangles = new int[xSize * zSize * 6];

        CreateVertices();
        CreateTriangles();
    }

    void CreateVertices()
    {
        for (int i = 0, z = 0; z <= zSize; z++, _newPerlinZ++)
        {
            for (int x = 0; x <= xSize; x++, _newPerlinX++)
            {
                float y = Mathf.PerlinNoise(_newPerlinX * .3f, _newPerlinZ * .3f) * 2f;
                _vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }
    }

    void CreateTriangles()
    {
        for (int vertex = 0, triangle = 0, j = 0; j < zSize; j++)
        {
            for (int i = 0; i < xSize; i++)
            {
                _triangles[triangle] = vertex + 0;
                _triangles[triangle + 1] = vertex + xSize + 1;
                _triangles[triangle + 2] = vertex + 1;

                _triangles[triangle + 3] = vertex + 1;
                _triangles[triangle + 4] = vertex + xSize + 1;
                _triangles[triangle + 5] = vertex + xSize + 2;

                vertex++;
                triangle += 6;
            }

            vertex++;
        }
    }

    void UpdateMesh()
    {
        Mesh.Clear();

        Mesh.vertices = _vertices;
        Mesh.triangles = _triangles;

        Mesh.RecalculateNormals();
        gameObject.AddComponent<MeshCollider>();
    }

    private void OnDrawGizmos()
    {
        if (_vertices == null)
            return;

        for (int i = 0; i < _vertices.Length; i++)
        {
            Gizmos.DrawSphere(_vertices[i], .1f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
