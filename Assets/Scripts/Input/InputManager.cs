using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private InputController _inputController;
    private GravityController _gravityController;
    private CouchController _crouchController;
    public Vector2 MovementInput;

    private void OnEnable()
    {
        _gravityController = GameObject.Find("Player").GetComponent<GravityController>();
        _crouchController = GameObject.Find("Player").GetComponent<CouchController>();
        if (_inputController == null)
        {
            _inputController = new InputController();

            _inputController.Player.Jump.performed += _gravityController.Jump;
            _inputController.Player.Crouch.performed += _crouchController.CrouchController;
            _inputController.Player.Crouch.canceled += _crouchController.CrouchCanceled;
        }

        _inputController.Enable();
    }

    private void OnDisable()
    {
        _inputController.Disable();
    }

    // Start is called before the first frame update
    /*void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }*/
}
