# M17UF3R1-RellanSanchezIvan

## Controls

Movement: WASD
Jump: Space
Crouch: Ctrl
Aim: Right Click
Dance: E
Use: Q
Run: Shift

Al pulsar Ctrl una vez, se quedará así mientras te muevas

## Objetivo

Encuentra cuatro objetos distintos en el escenario. La llave te permitirá superar el juego

## Obstáculos

Los pinchos y el agua hacen daño